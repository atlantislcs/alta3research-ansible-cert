The Ansible cert project file name: alta3research-ansiblecert01.yml
## This Ansible playbook only run in training lab environment, using bender as target machine. Just run the playbook and it will setup the web page in bender.
## Gitlab link for yml - https://gitlab.com/atlantislcs/alta3research-ansible-cert/-/raw/main/alta3research-ansiblecert01.yml
## Gitlab Link for README.md - https://gitlab.com/atlantislcs/alta3research-ansible-cert/-/raw/main/README.md
## Gitlab Link for entire cert project - https://gitlab.com/atlantislcs/alta3research-ansible-cert/-/tree/main/
